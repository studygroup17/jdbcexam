package com.greedy.section01.insert;

import static com.greedy.common.JDBCTemplate.getConnection; 
import static com.greedy.common.JDBCTemplate.close; 

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Application2 {

	public static void main(String[] args) {
		
		Connection con = getConnection();
		
		PreparedStatement pstmt = null;
		int result = 0;
		
		Properties prop = new Properties();
		
		try {
			prop.loadFromXML(new FileInputStream("mapper/menu-query.xml"));
			String query = prop.getProperty("insertMenu");
			
			Scanner sc = new Scanner(System.in);
			System.out.print("메뉴의 이름을 입력하세요: ");
			String menuName = sc.nextLine();
			System.out.print("메뉴의 가격을 입력하세요: ");
			int menuPrice = sc.nextInt();
			System.out.print("카테고리 코드를 입력하세요(4~12): ");
			int categoryCode = sc.nextInt();
			System.out.print("판매 여부를 결정해 주세요(Y/N): ");
			sc.nextLine();
			String orderableStatus = sc.nextLine().toUpperCase();	// check 제약조건을 생각해서 대문자로 바꿔주자(Y 또는 N만 들어가도록)
			
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, menuName);
			pstmt.setInt(2, menuPrice);
			pstmt.setInt(3, categoryCode);
			pstmt.setString(4, orderableStatus);
			
			result = pstmt.executeUpdate();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(con);
		}
		
		if(result > 0) {
			System.out.println("메뉴 등록 성공!");
		} else {
			System.out.println("메뉴 등록 실패");
		}
	}
}







