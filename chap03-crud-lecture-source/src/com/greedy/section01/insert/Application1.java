package com.greedy.section01.insert;

import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.close;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

public class Application1 {

	public static void main(String[] args) {
		Connection con = getConnection();
		
		PreparedStatement pstmt = null;
		int result = 0;		// DML을 진행하면 결과가 ResultSet이 아닌 int가 반환된다.
		
		Properties prop = new Properties();
		
		try {
			prop.loadFromXML(new FileInputStream("mapper/menu-query.xml"));
			String query = prop.getProperty("insertMenu");
			
			System.out.println("menu insert 쿼리: " + query);
			
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, "초코릿샤브샤브3");
			pstmt.setInt(2, 1000000);
			pstmt.setInt(3, 7);
			pstmt.setString(4, "Y");
			
			result = pstmt.executeUpdate();		// DML 쿼리를 수행할 때는 executeUpdate()이며 결과는 int(DML 작업이 이루어진 행의 갯수)다.
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(pstmt);
			close(con);				           // 트랜잭션인 commit이 발생함.
		}
		
		System.out.println("result: " + result);
		
	}

}
