package com.greedy.model.dto;

import java.io.Serializable;

public class CategoryDTO implements Serializable{
	private static final long serialVersionUID = -1319772209459856409L;
	
	private int code;
	private String name;
	private int refCode;
	
	public CategoryDTO() {
	}
	public CategoryDTO(int code, String name, int refCode) {
		this.code = code;
		this.name = name;
		this.refCode = refCode;
	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getRefCode() {
		return refCode;
	}
	public void setRefCode(int refCode) {
		this.refCode = refCode;
	}
	
	@Override
	public String toString() {
		return "CategoryDTO [code=" + code + ", name=" + name + ", refCode=" + refCode + "]";
	}
}
