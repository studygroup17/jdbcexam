package com.greedy.section01.view;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;
import com.greedy.section01.model.service.OrderService;

public class OrderMenu {

	private OrderService orderService = new OrderService();
	
	public void displayMainMenu() {

		/*
		 * 반복
		 * -------------------------------
		 * 1. 카테고리 조회
		 * 2. 해당 카테고리의 메뉴 조회
		 * 3. 사용자에게 어떤 메뉴를 주문받을 것인지 입력
		 * 4. 주문 할 수량 입력
		 * -------------------------------
		 * 5. 주문
		 */
		Scanner sc = new Scanner(System.in);
		
		int totalOrderPrice = 0; 									// 사용자가 고른 메뉴와 수량에 따른 주문 한건에 대한 총 금액을 저장할 변수 
		List<OrderMenuDTO> orderMenuList = new ArrayList<>();		// 사용자가 고른 메뉴와 수량(OrderMenuDTO)들을 저장할 List
		
		boolean flag = true;
		List<MenuDTO> menuList = null;
		do {
			System.out.println("============= 음식 주문 프로그램 ============");
			
			/* DB의 실시간으로 존재하는 카테고리 조회 후 화면에 출력 */
			List<CategoryDTO> categoryList = orderService.selectAllCategory();
			
			for(CategoryDTO cate : categoryList) {
				System.out.println(cate.getName());
			}
			
			/* 주문 할 카테고리 선택 */
			System.out.println("=========================================");
			System.out.println("주문하실 카테고리 종류를 입력해 주세요: ");
			String inputCategory = sc.nextLine();
			
			/* 사용자가 입력한 카테고리의 코드 번호 확인(가공) */
			int categoryCode = 0;
			for(int i = 0; i < categoryList.size(); i++) {
				CategoryDTO category = categoryList.get(i);
				if(category.getName().equals(inputCategory)) {
					categoryCode = category.getCode();
					break;
				}
			}
			
			/* 사용자가 입력한 카테고리에 해당하는 메뉴 조회 */
			System.out.println("=============== 주문 가능 메뉴 ===============");
			
			menuList = orderService.selectMenuBy(categoryCode);
			for(MenuDTO menu : menuList) {
				System.out.println(menu.getName() + ", " + menu.getPrice());
			}
			
			/* 사용자가 메뉴를 고르고 메뉴 코드 번호 및 가격 확인 */
			System.out.print("주문하실 메뉴를 선택해 주세요: ");
			String inputMenu = sc.nextLine();
			
			int menuCode = 0;
			int menuPrice = 0;
			for(int i = 0; i < menuList.size(); i++) {
				MenuDTO menu = menuList.get(i);
				if(menu.getName().equals(inputMenu)) {
					menuCode = menu.getCode();
					menuPrice = menu.getPrice();
					break;
				}
			}
			
			/* 해당 메뉴를 얼만큼 주문할 지 수량 확인 */
			System.out.print("주문하실 수량을 입력하세요: ");
			int orderAmount = sc.nextInt();
			
			OrderMenuDTO orderMenu = new OrderMenuDTO();	// OrderMenuDTO는 주문 시 하나의 메뉴에 대한 정보가 담기는 DTO
			orderMenu.setMenuCode(menuCode);
			orderMenu.setAmount(orderAmount);
						
			/* 하나의 메뉴에 대한 총금액을 알 수 있으므로 전체 총금액 변수에 누적하자 */
			totalOrderPrice += menuPrice * orderAmount;
			
			/* 하나의 메뉴에 대한 내용을 orderMenuList에 추가 */
			orderMenuList.add(orderMenu);
			
			/* 메뉴 반복 여부 판단 */
//			System.out.print("계속 주문하시겠습니까?(예/아니오): ");
//			sc.nextLine();
//			boolean isContinue = sc.nextLine().equals("예") ? true : false;
//			
//			if(!isContinue) {		// "예"를 선택하지 않았을 때 메뉴 반복을 멈추게 하자.
//				break;
//			}
			
			/* "예"는 다시 반복, "아니오"는 반복 중지, "그 외의 값"은 다시 계속 주문하시겠습니까?를 물어보게 해보자 */
			sc.nextLine();
			String isContinue = "";
			while(true) {
				System.out.print("계속 주문하시겠습니까?(예/아니오): ");
				isContinue = sc.nextLine();
				if("예".equals(isContinue)) {
					break;
				} else if("아니오".equals(isContinue)) {
					flag = false;
					break;
				}
			}
			
		} while(flag);
		
		/* 사용자가 고른 메뉴들에 대한 전체 내용 출력 */
		System.out.println("========= 총 주문 내역 =========");
		
		/* 
		 * 메뉴의 코드 번호로만 출력하는게 아쉬워 메뉴 이름을 보고 싶다면 MenuDTO를 
		 * 담고있는 List에서 코드번호로 메뉴를 찾아 이름을 확인할 수 있다. 
		 * (현재는 코드 번호로 또 select을 해서 메뉴 명을 알 필요가 없다. 왜냐면 MenuDTO를 이미 가져와서 알고 있기 때문에) 
		 * (이거하면서 List<MenuDTO> menuList 변수 선언은 반복문 밖으로 뺐음)
		 */
		for(OrderMenuDTO orderMenu : orderMenuList) {
//			System.out.println(orderMenu.getMenuCode());
			for(MenuDTO menu : menuList) {
				if(menu.getCode() == orderMenu.getMenuCode()) {
					System.out.println(menu.getName());
				}
			}
		}
		
		/* 사용자가 고른 메뉴들에 대한 총 주문 금액 */
		System.out.println("총 주문 금액: " + totalOrderPrice);
		
		/* 현재 주문시점의 주문일자와 주문시간을 가공처리 */
		java.util.Date orderTime = new java.util.Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yy/MM/dd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		String date = dateFormat.format(orderTime);
		String time = timeFormat.format(orderTime);
		
		/* 한 건의 주문(TBL_ORDER 테이블에 INSERT될 내용)과 관련된 모든 내용을 하나의 OrderDTO 인스턴스로 묶어줌 */
		OrderDTO order = new OrderDTO();
		order.setDate(date);
		order.setTime(time);
		order.setTotalOrderPrice(totalOrderPrice);
		
		/* 위의 모든 내용은 주문 한건과 관련된 내용이므로 OrderDTO로 한번에 묶어내자(OrderDTO에 속성을 추가하자) */
		order.setOrderMenuList(orderMenuList);
		
		/* service로 주문 관련 내용 insert실행을 위한 메소드 호출 */
		int result = orderService.registOrder(order);
		
		/* 메뉴 주문에 대한 정상흐름과 예외흐름 처리 */
		if(result > 0) {
			System.out.println("주문에 성공하셨습니다.");
		} else {
			System.out.println("주문에 실패하셨습니다.");
		}
	}

}







