package com.greedy.section01.model.service;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.commit;
import static com.greedy.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.List;

import com.greedy.section01.model.dao.OrderDAO;
import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;

public class OrderService {
	
	private OrderDAO orderDAO = new OrderDAO();

	/* 카테고리 전체 조회용 메소드 */
	public List<CategoryDTO> selectAllCategory() {

		/* 1. Connection 생성 */
		Connection con = getConnection();
		
		/* 2. DAO의 모든 카테고리 조회용 메소드를 Connection을 넘기며 결과 리턴받기 */
		List<CategoryDTO> categoryList = orderDAO.selectAllCategory(con);
		
		/* 3. Connection 닫기 */
		close(con);
		
		/* 4. 값 반환 */
		return categoryList;
	}

	/* 카테고리별 주문 가능한 메뉴 조회용 메소드 */
	public List<MenuDTO> selectMenuBy(int categoryCode) {
		
		/* 1. Connection 생성 */
		Connection con = getConnection();
		
		/* 2. DAO의 해당 카테고리 메뉴를 조회하는 메소드로 categoryCode를 Connection 객체와 같이 전달하여 조회 */
		List<MenuDTO> menuList = orderDAO.selectMenuBy(con, categoryCode);
		
		/* 3. Connection 닫기 */
		close(con);
		
		/* 4. 값 반환 */
		return menuList;
	}

	/* 주문 정보 등록용 메소드 */
	public int registOrder(OrderDTO order) {
		
		/* 1. Connection 생성 */
		Connection con = getConnection();
		
		/* 
		 * 2. 트랜잭션(하나의 일 처리 단위)의 성공 여부를 하나의 int 값으로 리턴할 변수 선언
		 *    (DML 작업이 2개 이상이라 그 결과를 하나의 변수로 저장하기 위한 용도)
		 *    (int 말고 boolean으로 처리할 수도 있다.)
		 */
		int result = 0;
		
		/* 3. DAO의 메소드로 앞에서 전달 받은 값과 Connection을 같이 넘겨서 insert 및 select 작업 실행 */
		/* 3-1. TBL_ORDER TABLE에 INSERT */
		int orderResult = orderDAO.registOrder(con, order);
		System.out.println("Service에서 TBL_Order 테이블에 INSERT 확인: " + orderResult);
		
		/* 3-2. 마지막 발생한 시퀀스 조회 */
		int orderCode = orderDAO.selectLastOrderCode(con);
		System.out.println("Service에서 마지막 주문 번호 확인: " + orderCode);
		
		/* 3-3. orderMenuList 안에 있는 각각의 OrderMenuDTO에 orderCode 추가(앞에서 입력 받지 못하고 3-2에서야 알게 된 값이므로) */
		List<OrderMenuDTO> orderMenuList = order.getOrderMenuList();
		for(int i = 0; i < orderMenuList.size(); i++) {
			OrderMenuDTO orderMenu = orderMenuList.get(i);
			orderMenu.setOrderCode(orderCode);
		}
		
		/* 3-4. TBL_ORDER_MENU 테이블에 List에 담긴 OrderMenuDTO 갯수만큼 insert를 하고 결과를 누적한다. */
		int orderMenuResult = 0;
		for(int i = 0; i < orderMenuList.size(); i++) {
			OrderMenuDTO orderMenu = orderMenuList.get(i);
			orderMenuResult += orderDAO.registOrderMenu(con, orderMenu);
		}
		
		/* 4. 성공 여부 판단 후 트랜잭션 처리 */
		if(orderResult > 0 && orderMenuResult == orderMenuList.size()) {
			commit(con);
			result = 1;
		} else {
			rollback(con);
		}
		
		/* 5. Connection 닫기 */
		close(con);
		
		/* 6. 값 반환 */
		return result;
	}

}











