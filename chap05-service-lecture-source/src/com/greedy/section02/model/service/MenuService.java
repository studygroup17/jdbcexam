package com.greedy.section02.model.service;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.commit;
import static com.greedy.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.greedy.section02.model.dao.MenuDAO;
import com.greedy.section02.model.dto.CategoryDTO;

/*
 * Service Layer(계층)의 역할
 * 1. Connection 생성
 * 2. DAO의 메소드 호출(Connection과 입력값 전달)
 * 3. 트랜잭션 제어
 * 4. Connection 닫기(반납)
 */
public class MenuService {

	/* 신규 메뉴 등록용 서비스 메소드 */
	public void registMenu() {		// Service 계층의 메소드 단위는 트랜잭션 단위이다.
		
		/* 1. Connection 생성 */
		Connection con = getConnection();		// Connection도 트랜잭션 단위이다.
		
		/* 2. DAO의 메소드 호출 */
		MenuDAO menuDAO = new MenuDAO();	// 쿼리를 불러오는 작업을 MenuDAO 기본생성자에 생성
		
		/* 2-1. 카테고리 등록 */
		CategoryDTO newCategory = new CategoryDTO();
		newCategory.setName("에피타이저");
		newCategory.setRefCode(null);		// DTO를 가서 refCode 속성의 자료형을 Integer로 해야 된다.
		
		int result1 = menuDAO.insertNewCategory(con, newCategory);
		System.out.println("카테고리 추가 여부: " + result1);
		
		/* 2-2. 방금 추가한 마지막 카테고리의 번호 조회 */
		int lastCategoryCode = menuDAO.selectLastCategoryCode(con);
		System.out.println("마지막 카테고리 번호: " + lastCategoryCode);
		
		/* 2-3. 신규 메뉴 등록 */
		Map<String, String> newMenu = new HashMap<>();
		newMenu.put("name", "콩자반김치국");
		newMenu.put("price", "5000");
		newMenu.put("category", Integer.valueOf(lastCategoryCode).toString());
		newMenu.put("orderable", "Y");
		
		int result2 = menuDAO.insertNewMenu(con, newMenu);
		
		/* 3. 트랜잭션 제어 */
		if(result1 > 0 && result2 > 0) {
			System.out.println("신규 카테고리와 메뉴를 모두 추가 성공!!");
			commit(con);
		} else {
			System.out.println("신규 카테고리와 메뉴를 모두 추가 실패!!");
			rollback(con);
		}
		
		/* 4. Connection 닫기 */
		close(con);
	}	

}






