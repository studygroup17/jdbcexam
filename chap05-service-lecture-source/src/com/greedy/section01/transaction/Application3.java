package com.greedy.section01.transaction;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.commit;
import static com.greedy.common.JDBCTemplate.rollback;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class Application3 {

	public static void main(String[] args) {
		Connection con = getConnection();
		try {
			System.out.println("autoCommit의 현재 설정값: " + con.getAutoCommit());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		PreparedStatement pstmt1 = null;
		Statement stmt = null;
		PreparedStatement pstmt2 = null;
		int result1 = 0;
		ResultSet rset = null;
		int result2 = 0;
		
		Properties prop = new Properties();
		
		try {
			
			prop.loadFromXML(new FileInputStream("mapper/menu-query.xml"));
			
			/* 1. 카테고리 추가 */
			String query1 = prop.getProperty("insertCategory");
			pstmt1 = con.prepareStatement(query1);
			pstmt1.setString(1, "군것질");
//			pstmt1.setInt(2, null);			// null이 아닌 숫자만 들어갈 경우
//			pstmt1.setObject(2, null);      // null을 넣고 싶고 숫자도 들어가고 싶은 경우(setObject를 쓴다.)
			pstmt1.setObject(2, 3);
			
			result1 = pstmt1.executeUpdate();
			
			/* 2. 카테고리 번호 조회 */
			String query3 = prop.getProperty("selectCurrentCategorySequenceValue");
			
			stmt = con.createStatement();
			rset = stmt.executeQuery(query3);
			
			int currentSequenceNum = 0;
			if(rset.next()) {
				currentSequenceNum = rset.getInt(1);
			}
			
			/* 3. 메뉴 추가 */
			String query2 = prop.getProperty("insertMenu");
			
			pstmt2 = con.prepareStatement(query2);
			pstmt2.setString(1, "정어리비빔밥");
			pstmt2.setInt(2, 5000);
			pstmt2.setInt(3, currentSequenceNum);
			pstmt2.setString(4, "Y");
			
			result2 = pstmt2.executeUpdate();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt1);
			close(stmt);
			close(pstmt2);
			
			/* Connection을 close 하기 전에 트랜잭션 작업을 수행한다. */
			if(result1 > 0 && result2 > 0) {
				commit(con);
			} else {
				rollback(con);
			}
			
			close(con);			// Connection을 close할 때 commit
		}
	
		/* 성공 실패 확인은 나중에 화면단에서 출력하자 */
	}
}




