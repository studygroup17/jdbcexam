package com.greedy.section02.template;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/* JDBC를 진행할 때 자주 쓰이는 기능을 공통 모듈로 분리 */
/* Connection 객체 만들고 close 관련 기능 담당 */
public class JDBCTemplate {
	public static Connection getConnection() {
		Connection con = null;
		
		Properties prop = new Properties();
		
		try {
			prop.load(new FileReader("config/connection-info.properties"));
			
			System.out.println(prop);
			String driver = prop.getProperty("driver");
			String url = prop.getProperty("url");
			
			Class.forName(driver);
			
			con = DriverManager.getConnection(url, prop);
			
			System.out.println("getConnection의 con: " + con);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return con;
	}
	
	/* Connection 인스턴스를 닫아주는 메소드(예외처리 대신 하는 용도) */
	public static void close(Connection con) {
		try {
			if(con != null && !con.isClosed()) {	// 넘어 온 Connection 인스턴스가 존재하고 닫혀있지 않을 때
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}










