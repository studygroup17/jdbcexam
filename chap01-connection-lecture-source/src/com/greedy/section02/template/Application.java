package com.greedy.section02.template;

/* 일반적인 import는 클래스명까지 */
import java.sql.Connection;
import java.sql.SQLException;

/* import static은 메소드명까지 */
import static com.greedy.section02.template.JDBCTemplate.getConnection;
import static com.greedy.section02.template.JDBCTemplate.close;

public class Application {

	public static void main(String[] args) {
//		Connection con = com.greedy.section02.template.JDBCTemplate.getConnection();
//		System.out.println("main에서의 con: " + con);
		
		/* 같은 패키지의 클래스이므로 */
//		Connection con = JDBCTemplate.getConnection();
//		System.out.println("main에서의 con: " + con);
		
		/* import static 방식으로 클래스명까지 생략 함 */
		Connection con = getConnection();
		System.out.println("main에서의 con: " + con);
		
		/* Connection 인스턴스를 활용해서 DB와의 CRUD를 위한 비즈니스 로직 실행(Connection 인스턴스 사용) */
		
		/* Connection 인스턴스 반납 시 일일히 try-catch로 감싸 주어야 한다. */
//		try {
//			con.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
		
		close(con);
		
	}

}










