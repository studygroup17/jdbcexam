package com.greedy.section01.statement;

import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.close;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Application1 {

	public static void main(String[] args) {
		
		 Connection con = getConnection();
//		 System.out.println("con 잘 되겠지? " + con);
		 
		 /* 쿼리문을 저장하고 실행하는 기능을 하는 용도의 인터페이스 */
		 Statement stmt = null;
		 
		 /* select 쿼리 실행 후 돌아오는 결과집합(Result Set)을 받아 줄 용도의 인터페이스 */
		 ResultSet rset = null;
		 
		 try {
			
			/* Connection 인스턴스로 Statement 인스턴스 생성 */
			stmt = con.createStatement();
			
			rset = stmt.executeQuery("SELECT A.EMP_ID, A.EMP_NAME 사원명 FROM EMPLOYEE A WHERE EMP_ID = '201'");
			
//			System.out.println("ResultSet 돌아오나? " + rset);
			while(rset.next()) {
				System.out.println(rset.getString("EMP_ID") + ", " + rset.getString("사원명"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			/* 닫는 순서는 ResultSet -> Statment -> Connection 순이다. */
			close(rset);
			close(stmt);
			close(con);		
		}
		 
	}

}











