package com.greedy.section02.preparedStatement;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.greedy.model.dto.EmployeeDTO;

public class Application5 {

	public static void main(String[] args) {
		Connection con = getConnection();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		EmployeeDTO row = null;
		List<EmployeeDTO> empList = null;
		
		Scanner sc = new Scanner(System.in);
		System.out.print("조회 할 사원의 성을 입력하세요: ");
		String lastName = sc.nextLine();
		
		String query = "SELECT * FROM EMPLOYEE WHERE EMP_NAME LIKE ? || '%'";
//		String query = "SELECT * FROM EMPLOYEE WHERE EMP_NAME LIKE '%' || ? || '%'";
		
		try {
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, lastName);
			
			rset = pstmt.executeQuery();
			
			empList = new ArrayList<>();
			while(rset.next()) {
//				System.out.println(rset.getString("EMP_NAME"));
				
				row = new EmployeeDTO();	// 한 행을 담을 DTO 인스턴스
				
				row.setEmpId(rset.getString("EMP_ID"));
				row.setEmpName(rset.getString("EMP_NAME"));
				row.setEmpNo(rset.getString("EMP_NO"));
				row.setEmail(rset.getString("EMAIL"));
				row.setPhone(rset.getString("PHONE"));
				row.setDeptCode(rset.getString("DEPT_CODE"));
				row.setJobCode(rset.getString("JOB_CODE"));
				row.setSalLevel(rset.getString("SAL_LEVEL"));
				row.setSalary(rset.getInt("SALARY"));
				row.setBonus(rset.getDouble("BONUS"));
				row.setManagerId(rset.getString("MANAGER_ID"));
				row.setHireDate(rset.getDate("HIRE_DATE"));
				row.setEntDate(rset.getDate("ENT_DATE"));
				row.setEntYn(rset.getString("ENT_YN"));
				
				empList.add(row);			// 한 행을 컬렉션에 추가하자.
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(rset);
			close(pstmt);
			close(con);
		}
		
		/* 컬렉션 출력하는 4가지 방법(복습) */
		/* 1. 컬렉션에서 제공하는 toString() */
		System.out.println(empList);
		
		/* 2. for문 */
		for(int i = 0; i < empList.size(); i++) {
			System.out.println(empList.get(i));
		}
		
		/* 3. for-each문 */
		for(EmployeeDTO emp : empList) {
			System.out.println(emp);
//			System.out.println(emp.getEmpId() + ", " + emp.getEmpName());
		}
		
		/* 4. iterator */
		Iterator<EmployeeDTO> iter = empList.iterator();
		while(iter.hasNext()) {
			System.out.println(iter.next());
		}
	}
}




