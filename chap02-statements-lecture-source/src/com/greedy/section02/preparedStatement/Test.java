package com.greedy.section02.preparedStatement;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/* 쿼리를 저장 할 xml 파일 생성용 예제 */
public class Test {

	public static void main(String[] args) {
		
		Properties prop = new Properties();
		prop.setProperty("keyString", "valueString");
		
		try {
			prop.storeToXML(
					new FileOutputStream("src/com/greedy/section02/preparedStatement/employee-query.xml"), "title");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}







