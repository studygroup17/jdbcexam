package com.greedy.section01.model.service;

import static com.greedy.common.JDBCTemplate.close;
import static com.greedy.common.JDBCTemplate.getConnection;
import static com.greedy.common.JDBCTemplate.commit;
import static com.greedy.common.JDBCTemplate.rollback;

import java.sql.Connection;
import java.util.List;

import com.greedy.section01.model.dao.OrderDAO;
import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;

public class OrderService {
	
	private OrderDAO orderDAO = new OrderDAO();

	public List<CategoryDTO> selectAllCategory() {
		Connection con = getConnection();
		
		List<CategoryDTO> categoryList = orderDAO.selectAllCategory(con);
		
		close(con);
		
		return categoryList;
	}

	public List<MenuDTO> selectMenuBy(int categoryCode) {
		Connection con = getConnection();
		
		List<MenuDTO> menuList = orderDAO.selectMenuBy(con, categoryCode);
		
		close(con);
		
		return menuList;
	}

	public int registOrder(OrderDTO order) {
		
		/* 1. Connection 생성 */
		Connection con = getConnection();
		
		/* 2. 트랜잭션 결과(최종결과)를 담을 하나의 변수 */
		int result = 0;
		
		/* 3-1. ORDER TABLE INSERT */
		int orderResult = orderDAO.registOrder(con, order);
		
		/* 3-2. 마지막 주문 시퀀스 SELECT */
		int orderCode = orderDAO.selectLastOrderCode(con);
		
		/* 3-3. ORDER_MENU TABLE INSERT */
		/* 주문 번호를 이제 알았으니 채우는 작업을 하자 */
		List<OrderMenuDTO> orderMenuList = order.getOrderMenuList();
		for(int i = 0; i < orderMenuList.size(); i++) {
			OrderMenuDTO orderMenu = orderMenuList.get(i);
			orderMenu.setOrderCode(orderCode);
		}
		
		int orderMenuResult = 0;
		for(int i = 0; i < orderMenuList.size(); i++) {
			OrderMenuDTO orderMenu = orderMenuList.get(i);
			orderMenuResult += orderDAO.registOrderMenu(con, orderMenu);
		}
		
		/* 4. 트랜잭션 처리 */
		if(orderResult > 0 && orderMenuResult == orderMenuList.size()) {
			commit(con);
			result = 1;
		} else {
			rollback(con);
		}
		
		/* 5. Connection 닫기 */
		close(con);
		
		/* 6. 최종 결과 값 반환 */
		return result;
	}
}








