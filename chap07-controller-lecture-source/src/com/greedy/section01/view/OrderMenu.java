package com.greedy.section01.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.greedy.section01.controller.OrderController;
import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;

public class OrderMenu {

	private OrderController orderController = new OrderController();
	
	public void displayMainMenu() {

		/* 누적 시킬 값을 담을 변수들 선언 */
		List<OrderMenuDTO> orderMenuList = new ArrayList<>();
		int totalOrderPrice = 0;
		
		Scanner sc = new Scanner(System.in);
		
		boolean flag = true;
		do {
			System.out.println("=========== 음식 주문 프로그램 ===========");
			
			List<CategoryDTO> categoryList = orderController.selectAllCategory();
			for(CategoryDTO cate : categoryList) {
				System.out.println(cate);
			}
			
			System.out.println("=====================================");
			System.out.print("주문하실 카테고리 종류의 이름을 입력해 주세요: ");
			String inputCategory = sc.nextLine();
			
			System.out.println("=========== 주문 가능 메뉴 ============");
			
			/* 입력받은 값 가공 처리는 Controller에게 맡기고 가공 처리에 필요한 값을 넘긴다. */
			List<MenuDTO> menuList = orderController.selectMenuBy(inputCategory, categoryList);
			for(MenuDTO menu : menuList) {
				System.out.println(menu);
			}
			
			System.out.print("주문하실 메뉴를 선택해 주세요: ");
			String inputMenu = sc.nextLine();
			
			/* Controller에서 처리할 수 없는 가공처리는 화면단에서 처리한다.(누적값은 화면단에서 처리되야 되므로) */
			int menuCode = 0;
			int menuPrice = 0;
			for(int i = 0; i < menuList.size(); i++) {
				MenuDTO menu = menuList.get(i);
				if(menu.getName().equals(inputMenu)) {
					menuCode = menu.getCode();
					menuPrice = menu.getPrice();
				}
			}
			
			System.out.print("주문하실 수량을 입력하세요: ");
			int orderAmount = sc.nextInt();
			
			totalOrderPrice += menuPrice * orderAmount;
			
			OrderMenuDTO orderMenu = new OrderMenuDTO();
			orderMenu.setMenuCode(menuCode);
			orderMenu.setAmount(orderAmount);
			
			orderMenuList.add(orderMenu);
			
			sc.nextLine();
			String isContinue = "";
			while(true) {
				System.out.print("계속 주문하시겠습니까?(예/아니오): ");
				isContinue = sc.nextLine();
				if("예".equals(isContinue)) {
					break;
				} else if("아니오".equals(isContinue)) {
					flag = false;
					break;
				}
			}
			
		} while(flag);
		
		/* 전체 주문 관련 내용을 이번에는 Map(DTO 말고)에 담아 Controller로 전달 */
		Map<String, Object> requestMap = new HashMap<>();
		requestMap.put("totalOrderPrice", totalOrderPrice);
		requestMap.put("orderMenuList", orderMenuList);
		
		orderController.registOrder(requestMap);
	}
}








