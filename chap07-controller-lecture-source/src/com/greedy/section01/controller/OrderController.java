package com.greedy.section01.controller;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import com.greedy.section01.model.dto.CategoryDTO;
import com.greedy.section01.model.dto.MenuDTO;
import com.greedy.section01.model.dto.OrderDTO;
import com.greedy.section01.model.dto.OrderMenuDTO;
import com.greedy.section01.model.service.OrderService;
import com.greedy.section01.view.ResultView;

/*
 * Controller Layer(계층)의 역할
 * 1. 사용자가 입력한 정보를 파라미터 형태로 전달받아 전달받은 값을 검증하거나 가공
 * 2. Service의 메소드 호출
 * 3. 수행 결과를 반환 받아 사용자에게 보여줄 뷰 걸정
 * 4. 뷰에 필요한 데이터를 전달
 */
public class OrderController {

	private OrderService orderService = new OrderService();

	public List<CategoryDTO> selectAllCategory() {
		return orderService.selectAllCategory();
	}

	public List<MenuDTO> selectMenuBy(String inputCategory, List<CategoryDTO> categoryList) {
		int categoryCode = 0;
		for(int i = 0; i < categoryList.size(); i++) {
			CategoryDTO category = categoryList.get(i);
			if(category.getName().equals(inputCategory)) {
				categoryCode = category.getCode();
				break;
			}
		}
		
		return orderService.selectMenuBy(categoryCode);
	}

	public void registOrder(Map<String, Object> requestMap) {
		
		/* 1. 뷰에서 전달 받은 파라미터를 꺼내서 각각의 변수에 담기 */
		int totalOrderPrice = (Integer)requestMap.get("totalOrderPrice");
		List<OrderMenuDTO> orderMenuList = (List<OrderMenuDTO>)requestMap.get("orderMenuList");
		
		/* 2. 화면단에서 미처 제공하지 못했거나 가공처리해야 할 것들을 가공처리 하자 */
		/* 2-1. 주문 날짜와 시간 구하기 */
		java.util.Date orderTime = new java.util.Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yy/MM/dd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		String date = dateFormat.format(orderTime);
		String time = timeFormat.format(orderTime);
		
		/* 2-2. 서비스 쪽으로 전달하기 위해 DTO 인스턴스로 만들기(묶기) */
		OrderDTO order = new OrderDTO();
		order.setDate(date);
		order.setTime(time);
		order.setTotalOrderPrice(totalOrderPrice);
		order.setOrderMenuList(orderMenuList);
		
		/* 3. Service 메소드를 호출하고 결과를 리턴받음 */
		int result = orderService.registOrder(order);
		
		/* 4. Service 처리 결과를 이용해서 성공 실패 여부를 판단하여 사용자에게 보여줄 뷰를 결정하고 뷰에 필요한 값을 전달함 */
		ResultView resultView = new ResultView();
		if(result > 0) {
			resultView.success(orderMenuList.size());
		} else {
			resultView.failed();
		}
	}
	
	
	
}


